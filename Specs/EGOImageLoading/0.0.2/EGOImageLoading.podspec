Pod::Spec.new do |spec|
  spec.name         = 'EGOImageLoading'
  spec.version      = '0.0.2'
  spec.platform     = :ios
  spec.license      = 'MIT'
  spec.summary      = 'What if images on the iPhone were as easy as HTML?'
  spec.homepage     = 'https://github.com/enormego/EGOImageLoading'
  spec.author       = 'Shaun Harrison'
  spec.source       = { :git => 'https://bitbucket.org/cleverpumpkin/EGOImageLoading',
                        :tag => 'v' + spec.version.to_s }
  spec.requires_arc = false
  spec.source_files = 'EGO*/*.{h,m}'

  spec.dependency 'EGOCache'
end
