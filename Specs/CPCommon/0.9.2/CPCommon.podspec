class Pod::Specification
	def cp_subspec (name, useCommonFiles: false, cpcommonDependencies: [], useSwift: true, &block)
		self.subspec name do |sspec|
			if useCommonFiles
				sspec.subspec 'Common' do |sspec_common|
					sspec_common.source_files = "#{sspec.name}/Common/*.{h,m}"
				end
			end

			sspec.subspec 'ObjC' do |sspec_objc|
				sspec_objc.source_files = "#{sspec.name}/ObjC/*.{h,m}"
				sspec_objc.pod_target_xcconfig = {
					'SWIFT_ACTIVE_COMPILATION_CONDITIONS' => "#{sspec.name.gsub('/', '_').upcase}_OBJC"
				}
				sspec_objc.user_target_xcconfig = {
					'SWIFT_ACTIVE_COMPILATION_CONDITIONS' => "#{sspec.name.gsub('/', '_').upcase}_OBJC"
				}
				
				sspec_objc.dependency "#{self.name}/Core"
				if useCommonFiles
					sspec_objc.dependency "#{sspec.name}/Common"
				end
				cpcommonDependencies.each do |dependency|
					sspec_objc.dependency "#{self.name}/#{dependency}/ObjC"
				end
			end
			
			if useSwift
				sspec.subspec 'Swift' do |sspec_swift|
					sspec_swift.source_files = "#{self.name}/#{name}/Swift/*.swift"
					sspec_swift.pod_target_xcconfig = {
						'GCC_PREPROCESSOR_DEFINITIONS' => "#{sspec.name.gsub('/', '_').upcase}_SWIFT=1"
					}
					sspec_swift.user_target_xcconfig = {
						'GCC_PREPROCESSOR_DEFINITIONS' => "#{sspec.name.gsub('/', '_').upcase}_SWIFT=1"
					}

					sspec_swift.dependency "#{self.name}/Core"
					if useCommonFiles
						sspec_swift.dependency "#{sspec.name}/Common"
					end
					cpcommonDependencies.each do |dependency|
						sspec_swift.dependency "#{self.name}/#{dependency}/Swift"
					end
				end
			end

			sspec.frameworks = 'Foundation'
			
			yield sspec if block_given?
		end
	end
end

Pod::Spec.new do |spec|
	spec.name                  = 'CPCommon'
	spec.version               = '0.9.2'
	spec.license               = { :type => 'Proprietary', :text => 'Copyright © 2017 Cleverpumpkin, Ltd. All rights reserved.' }
	spec.homepage              = "https://bitbucket.org/cleverpumpkin/#{spec.name.downcase}-ios"
	spec.authors               = { 'Kirill Bystrov' => 'kirill@cleverpumpkin.ru' }
	spec.summary               = 'Common reusable classes implemented both in Swift and Objective-C.'
	spec.source                = { :git => spec.homepage, :tag => "v#{spec.version}" }
	spec.requires_arc          = true
	spec.ios.deployment_target = 9.3
	spec.module_map            = "#{spec.name}/Supporting Files/#{spec.name}.modulemap"
	
	spec.subspec 'Core' do |sspec|
		sspec.source_files         = "#{spec.name}/Supporting Files/#{spec.name}.{h,m}"
		sspec.platform             = :ios
		sspec.pod_target_xcconfig  = {
			'DYLIB_CURRENT_VERSION' => spec.version.major.to_s,
			'DYLIB_COMPATIBILITY_VERSION' => "#{spec.version.major}.#{spec.version.minor}",
			'CURRENT_PROJECT_VERSION' => spec.version.to_s,
			'VERSION_INFO_FILE' => '',
			'GCC_PREPROCESSOR_DEFINITIONS' => 'PRODUCT_VERSION_NUMBER=${DYLIB_COMPATIBILITY_VERSION} PRODUCT_VERSION_STRING=\"${CURRENT_PROJECT_VERSION}\"  CPCOMMON_HUMAN_READABLE_VERSION=\"${CURRENT_PROJECT_VERSION}\"'
		}
	end
	
	spec.cp_subspec 'CPFoundation', useCommonFiles: true

	spec.cp_subspec 'CPKeychainWrapper', cpcommonDependencies: ["CPFoundation"] do |sspec|
		sspec.frameworks = 'Security'
	end
	
	spec.cp_subspec 'CPWebCredentials', cpcommonDependencies: ["CPFoundation"] do |sspec|
		sspec.frameworks = 'Security'
	end
	
	spec.cp_subspec 'CPDeviceID', cpcommonDependencies: ["CPKeychainWrapper"], useSwift: false

	spec.cp_subspec 'CPCoreData', cpcommonDependencies: ["CPFoundation"] do |sspec|
		sspec.frameworks = 'CoreData'
	end
	
	spec.cp_subspec 'CPTheming', cpcommonDependencies: ["CPFoundation"], useSwift: false do |sspec|
		sspec.frameworks = 'UIKit'
	end

	spec.cp_subspec 'CPUIKit', cpcommonDependencies: ["CPFoundation"] do |sspec|
		sspec.frameworks = 'UIKit'
	end
	
	spec.subspec 'ObjC' do |sspec|
		sspec.dependency 'CPCommon/CPFoundation/ObjC'
		sspec.dependency 'CPCommon/CPKeychainWrapper/ObjC'
		sspec.dependency 'CPCommon/CPWebCredentials/ObjC'
		sspec.dependency 'CPCommon/CPDeviceID/ObjC'
		sspec.dependency 'CPCommon/CPCoreData/ObjC'
		sspec.dependency 'CPCommon/CPTheming/ObjC'
		sspec.dependency 'CPCommon/CPUIKit/ObjC'
	end
	
	spec.subspec 'Swift' do |sspec|
		sspec.dependency 'CPCommon/CPFoundation/Swift'
		sspec.dependency 'CPCommon/CPKeychainWrapper/Swift'
		sspec.dependency 'CPCommon/CPWebCredentials/Swift'
		sspec.dependency 'CPCommon/CPDeviceID/ObjC'
		sspec.dependency 'CPCommon/CPCoreData/Swift'
		sspec.dependency 'CPCommon/CPTheming/ObjC'
		sspec.dependency 'CPCommon/CPUIKit/Swift'
	end
end
