class CPSubspec
	def self.parse (flags)
		return FLAGS.keys.map { |flag| self.parse_flags flags, flag }.keep_if { |v| v }
	end

	def write (spec, selfDeps)
		spec.subspec self.name do |sspec|
			sspec.source_files = "#{sspec.name}/#{self.files_pattern}"
			sspec.private_header_files = "#{sspec.name}/#{self.protected_files_pattern}" if self.privateFiles
			
			if self.xccfgTemplate and not self.xccfgTemplate.empty?
				sspec_cfg_name = sspec.name.gsub('/', '_').upcase
				xccfg = self.xccfgTemplate.map { |var, value| { var => value % sspec_cfg_name } }.reduce (:merge)
				
				sspec.pod_target_xcconfig = xccfg
				sspec.user_target_xcconfig = xccfg
			end
			
			sspec.dependency "#{spec.name}/Common" if self.commonDep
			selfDeps.each do |selfDep|
				sspec.dependency "#{spec.root.name}/#{selfDep}/#{self.name}"
			end
		end
	end

protected
	unless defined? FLAGS
		FLAGS = {
			'c' => {
				:name => 'Common',
				:extensions => ['h', 'c', 'm'],
			},
			'o' => {
				:name => 'ObjC',
				:extensions => ['h', 'c', 'm'],
				:xccfgTemplate => {
					'SWIFT_ACTIVE_COMPILATION_CONDITIONS' => '%s',
				},
			},
			's' => {
				:name => 'Swift',
				:extensions => ['swift'],
				:xccfgTemplate => {
					'GCC_PREPROCESSOR_DEFINITIONS' => '%s=1',
				},
			},
		}
		COMMON_FLAG = 'c'
		PRIVATE_FLAG = 'p'

		AGGREGATE_SUBSPECS = FLAGS.map { |flag, attrs| attrs [:name] if flag != COMMON_FLAG }.keep_if { |v| v }
		public_constant :AGGREGATE_SUBSPECS
	end

	def self.parse_flags (flags, flag)
		unless attrs = FLAGS[flag] and idx = flags.index(flag)
			return nil
		end
		
		attrs[:privateFiles] = (flags.slice(idx + 1) == PRIVATE_FLAG)
		attrs[:hasCommon] = flags.include? COMMON_FLAG
		return self.new attrs
	end

	attr_reader :name
	attr_reader :extensions
	attr_reader :privateFiles
	attr_reader :xccfgTemplate
	attr_reader :commonDep

	def initialize (attrs)
		@name = attrs [:name]
		@extensions = attrs [:extensions]
		@privateFiles = attrs [:privateFiles]
		@xccfgTemplate = attrs [:xccfgTemplate]
		@commonDep = (attrs [:hasCommon] and attrs [:name] != 'Common')
	end

	def files_pattern ()
		if self.extensions.empty?
			return "*.*"
		elsif self.extensions.length == 1
			return "*.#{self.extensions.first}"
		else
			return "*.{#{self.extensions.join(',')}}"
		end
	end

	def protected_files_pattern ()
		return self.files_pattern.gsub(/^\*\./, '*_Protected.')
	end
end

class Pod::Specification
	def cp_subspec (name, flags: 'os', selfDeps: [], frameworks: ['Foundation'], &block)
		subspecs = CPSubspec.parse (flags)
		self.subspec name do |sspec|
			subspecs.each do |subspec|
				subspec.write(sspec, [selfDeps].flatten)
			end

			sspec.dependency "#{self.name}/Core"
			sspec.frameworks = [frameworks].flatten
			
			yield sspec if block_given?
		end
	end
	
	def cp_add_aggregates ()
		CPSubspec::AGGREGATE_SUBSPECS.each do |name|
			self.cp_add_aggregate name
		end
	end

protected
	def cp_add_aggregate (name)
		self.subspec name do |sspec|
			self.cp_aggregate_deps(name).each do |dep|
				sspec.dependency dep
			end
		end
	end
	
	def cp_aggregate_deps (name)
		return self.subspecs.map { |sspec|
			sspec_name = sspec.name.split('/').last
			next nil if CPSubspec::AGGREGATE_SUBSPECS.include? sspec_name
			next nil if (sspec_name == 'Core')
			sspec.subspec_named(name).name rescue "#{sspec.name}/ObjC"
		}.keep_if { |v| v }
	end
	
	def subspec_named (name)
		return self.subspecs.find { |sspec| sspec.name.end_with? "/#{name}" }
	end
end

Pod::Spec.new do |spec|
	spec.name                  = 'CPCommon'
	spec.version               = '1.1.4.4'
	spec.license               = { :type => 'Proprietary', :text => 'Copyright © 2017 Cleverpumpkin, Ltd. All rights reserved.' }
	spec.homepage              = "https://bitbucket.org/cleverpumpkin/#{spec.name.downcase}-ios"
	spec.authors               = { 'Kirill Bystrov' => 'kirill@cleverpumpkin.ru' }
	spec.summary               = 'Common reusable classes implemented both in Swift and Objective-C.'
	spec.source                = { :git => "git@bitbucket.org:cleverpumpkin/#{spec.name.downcase}-ios", :tag => "v#{spec.version}", :submodules => true }
	spec.requires_arc          = true
	spec.ios.deployment_target = 9.3
	spec.module_map            = "#{spec.name}/Supporting Files/#{spec.name}.modulemap"
	spec.prepare_command       = '"CPCommon/Tools/Scripts/prepare.sh"'

	spec.subspec 'Core' do |sspec|
		sspec.source_files         = "#{sspec.name}/#{spec.name}.{h,m}"
		sspec.preserve_path        = 'CPCommon/Tools/ABIcons/ABIcons'
		sspec.platform             = :ios
		sspec.pod_target_xcconfig  = {
			'_XC_SLASH' => '/',
			'_XC_DBL_SLASH' => '${_XC_SLASH}${_XC_SLASH}',
			'DYLIB_CURRENT_VERSION' => "#{spec.version.major}",
			'DYLIB_COMPATIBILITY_VERSION' => "#{spec.version.major}.#{spec.version.minor}",
			'CURRENT_PROJECT_VERSION' => "#{spec.version}",
			'VERSION_INFO_EXPORT_DECL' => '${_XC_DBL_SLASH}',
			'SWIFT_ACTIVE_COMPILATION_CONDITIONS' => '$(inherited)',
			'GCC_PREPROCESSOR_DEFINITIONS' => [
				"PRODUCT_VERSION_NUMBER=#{spec.version.major}.#{spec.version.minor}",
				'PRODUCT_VERSION_STRING=\"' + "#{spec.version}" + '\"',
				'CPCOMMON_HUMAN_READABLE_VERSION=\"' + "#{spec.version}" + '\"',
			].join(' '),
		}
		sspec.user_target_xcconfig = {
			'SWIFT_ACTIVE_COMPILATION_CONDITIONS' => '$(inherited)',
		}
	end
	
	spec.cp_subspec 'CPFoundation', flags: 'cpos'
	spec.cp_subspec 'CPKeychainWrapper', selfDeps: 'CPFoundation', frameworks: 'Security'
	spec.cp_subspec 'CPWebCredentials', selfDeps: 'CPFoundation', frameworks: 'Security'
	spec.cp_subspec 'CPDeviceID', flags: 'o', selfDeps: 'CPKeychainWrapper'
	spec.cp_subspec 'CPCoreData', selfDeps: 'CPFoundation', frameworks: 'CoreData'
	spec.cp_subspec 'CPTheming', flags: 'o', selfDeps: 'CPFoundation', frameworks: 'UIKit'
	spec.cp_subspec 'CPUIKit', flags: 'cpos', selfDeps: 'CPFoundation', frameworks: 'UIKit' do |sspec|
		sspec.dependency 'MBProgressHUD', '~> 1.0'
	end
	
	spec.cp_add_aggregates
end
