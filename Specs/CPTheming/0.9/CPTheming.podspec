Pod::Spec.new do |spec|
	spec.name                 = 'CPTheming'
	spec.version              = '0.9'
	spec.license              = { :type => 'Proprietary', :text => 'Copyright © 2016 Cleverpumpkin, Ltd. All rights reserved.' }
	spec.homepage             = 'https://bitbucket.org/cleverpumpkin/' + spec.name.downcase
	spec.authors              = { 'Kirill byss Bystrov' => 'kirill@cleverpumpkin.ru' }
	spec.summary              = 'Simple library for easy controls theming.'
	spec.source               = { :git => spec.homepage + '.git', :tag => 'v' + spec.version.to_s }
	spec.requires_arc         = true
	spec.source_files         = 'CPTheming/Core/*.{h,m}', 'CPTheming/UIKit/*.{h,m}'
	
	spec.ios.deployment_target = '9.0'
	spec.ios.frameworks = 'UIKit'
end
