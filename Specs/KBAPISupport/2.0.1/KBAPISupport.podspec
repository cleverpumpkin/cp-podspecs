Pod::Spec.new do |spec|
	spec.name                 = 'KBAPISupport'
	spec.version              = '2.0.1'
	spec.license              = { :type => 'MIT' }
	spec.homepage             = 'https://github.com/byss/' + spec.name
	spec.authors              = { 'Kirill byss Bystrov' => 'kirrbyss@gmail.com' }
	spec.summary              = 'Simple library for HTTP/HTTPS requests and parsing & mapping JSON/XML responses to native objects.'
	spec.source               = { :git => spec.homepage + '.git', :tag => 'v' + spec.version.to_s }
	spec.source_files         = 'KBAPISupport/*.{h,m}', 'KBAPISupport/ConfigBits/_KB_FOOTER.h'
	spec.private_header_files = 'KBAPISupport/ARCSupport.h'
	spec.requires_arc         = true
	
	spec.subspec 'Debug' do |sspec_debug|
		sspec_debug.source_files = 'KBAPISupport/ConfigBits/_KB_DEBUG.h'
	end
	spec.subspec 'JSON' do |sspec_json|
		sspec_json.source_files = 'KBAPISupport/ConfigBits/_KB_JSON.h'
	end
	spec.subspec 'XML' do |sspec_xml|
		sspec_xml.source_files = 'KBAPISupport/ConfigBits/_KB_XML.h'
		sspec_xml.dependency 'GDataXML-HTML', '~> 1.2.0'
		sspec_xml.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
	end
	spec.subspec 'Decode_CP1251' do |sspec_decode_1251|
		sspec_decode_1251.source_files = 'KBAPISupport/ConfigBits/_KB_DECODE_CP1251.h'
	end
	spec.subspec 'Delegates' do |sspec_delegates|
		sspec_delegates.source_files = 'KBAPISupport/ConfigBits/_KB_DELEGATES.h'
	end
	spec.subspec 'Blocks' do |sspec_blocks|
		sspec_blocks.source_files = 'KBAPISupport/ConfigBits/_KB_BLOCKS.h'
	end
	
	spec.subspec 'JSON+Delegates' do |sspec_json_delegates|
		sspec_json_delegates.dependency 'KBAPISupport/JSON'
		sspec_json_delegates.dependency 'KBAPISupport/Delegates'
	end
	spec.subspec 'JSON+Delegates' do |sspec_json_blocks|
		sspec_json_blocks.dependency 'KBAPISupport/JSON'
		sspec_json_blocks.dependency 'KBAPISupport/Blocks'
	end
	spec.subspec 'JSON+Delegates' do |sspec_xml_delegates|
		sspec_xml_delegates.dependency 'KBAPISupport/XML'
		sspec_xml_delegates.dependency 'KBAPISupport/Delegates'
	end
	
	spec.prepare_command = <<-EOF
		pushd KBAPISupport &&
		./KBAPISupport-Pods-genconfig.sh > KBAPISupport-config.h &&
		./KBAPISupport-prepare.sh &&
		popd
	EOF
end
