Pod::Spec.new do |spec|
	spec.name                   = 'CPDeviceID'
	spec.version                = '1.1.1'
	spec.license                = { :type => 'Proprietary', :text => 'Copyright (c) 2016 Cleverpumpkin Ltd. All rights reserved.' }
	spec.homepage               = "https://bitbucket.org/cleverpumpkin/#{spec.name.downcase}"
	spec.authors                = { 'Kirill byss Bystrov' => 'kirill@cleverpumpkin.ru' }
	spec.summary                = 'Keychain wrapper.'
	spec.source                 = { :git => "git@bitbucket.org:cleverpumpkin/#{spec.name.downcase}.git", :tag => "v#{spec.version.to_s}" }
	spec.requires_arc           = true
	spec.ios.deployment_target  = '9.3'
	spec.deprecated_in_favor_of = 'CPCommon/CPDeviceID'

	spec.dependency 'CPKeychainWrapper', '~> 1.0'

	spec.subspec 'Core' do |sspec|
		sspec.source_files = 'CPDeviceID/Core/*.{h,m}'
		sspec.frameworks = 'Foundation'
	end

	spec.subspec 'DeviceName' do |sspec|
		sspec.source_files = 'CPDeviceID/DeviceName/*.{h,m}'
 		sspec.dependency 'CPDeviceID/Core'
		sspec.ios.frameworks = 'UIKit'
		sspec.ios.deployment_target = '9.3'
	end

	spec.default_subspecs = 'Core', 'DeviceName'
end
