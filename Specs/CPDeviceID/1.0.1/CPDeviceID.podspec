Pod::Spec.new do |spec|
	spec.name                 = 'CPDeviceID'
	spec.version              = '1.0.1'
	spec.license              = { :type => 'Proprietary', :text => 'Copyright (c) 2016 Cleverpumpkin Ltd. All rights reserved.' }
	spec.homepage             = 'https://bitbucket.org/cleverpumpkin/' + spec.name.downcase
	spec.authors              = { 'Kirill byss Bystrov' => 'kirill@cleverpumpkin.ru' }
	spec.summary              = 'Keychain wrapper.'
	spec.source               = { :git => spec.homepage + '.git', :tag => 'v' + spec.version.to_s }
	spec.requires_arc         = true
	
	spec.dependency 'CPKeychainWrapper', '~> 1.0'

	spec.ios.deployment_target     = '4.0'
	spec.osx.deployment_target     = '10.6'
	spec.watchos.deployment_target = '2.0'

	spec.subspec 'Core' do |sspec|
		sspec.source_files = 'CPDeviceID/Core/*.{h,m}'
		sspec.frameworks = 'Foundation'
	end

	spec.subspec 'DeviceName' do |sspec|
		sspec.source_files = 'CPDeviceID/DeviceName/*.{h,m}'
 		sspec.dependency 'CPDeviceID/Core'
		sspec.ios.frameworks = 'UIKit'
		sspec.ios.deployment_target = '4.0'
	end

	spec.default_subspecs = 'Core', 'DeviceName'
end
