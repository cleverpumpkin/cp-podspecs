Pod::Spec.new do |spec|
	spec.name                   = 'CPKeychainWrapper'
	spec.version                = '1.0.0'
	spec.license                = { :type => 'Proprietary', :text => 'Copyright (c) 2016 Cleverpumpkin Ltd. All rights reserved.' }
	spec.homepage               = "https://bitbucket.org/cleverpumpkin/#{spec.name.downcase}"
	spec.authors                = { 'Kirill byss Bystrov' => 'kirill@cleverpumpkin.ru' }
	spec.summary                = 'Keychain wrapper.'
	spec.source                 = { :git => "git@bitbucket.org:cleverpumpkin/#{spec.name.downcase}.git", :tag => "v#{spec.version.to_s}" }
	spec.requires_arc           = true
	spec.source_files           = 'CPKeychainWrapper/CPKeychainWrapper.{h,m}'
	spec.deprecated_in_favor_of = 'CPCommon/CPKeychainWrapper'
	
	spec.ios.deployment_target     = '4.0'
	spec.osx.deployment_target     = '10.6'
	spec.watchos.deployment_target = '2.0'
	spec.frameworks = 'Foundation', 'Security'
end
